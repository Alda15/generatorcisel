package sample.enums;

public enum Operators {

    TEST(822),
    TM(811),
    O2(812),
    VODAFONE(813),
    NORDIC(814),
    exGTS(7234);

    private int value;

        Operators(int value) {
            this.value = value;
        }

    public int getNumber () {
        return value;
    }

}

/*
TEST("M_TEST", 822),
    TM("M_TM", 811),
    O2("M_O2", 812),
    VODAFONE("M_VODAFONE", 813),
    NORDIC("M_NORDIC", 814),
    exGTS("F_GTS", 7234);
* */