package sample.enums;

public enum Database {

    QAP1("jdbc:oracle:thin:@//rztpowb06.cz.tmo:1584/QAP01.WORLD", "apptest", "ittc"),
    QAP2("jdbc:oracle:thin:@//hkpowm03.cz.tmo:1684/Q2AP01.WORLD", "apptest", "ittc"),
    COM1("jdbc:oracle:thin:@//rztvnode026.cz.tmo:1525/QCOM01.WORLD", "apptest", "dm1v93gtr"),
    COM2("jdbc:oracle:thin:@//rztvnode005.cz.tmo:1525/Q2COM01.WORLD", "apptest", "Paegas0303"),
    SBL1("jdbc:oracle:thin:@//rztvnode026.cz.tmo:1523/QSBL01.WORLD", "EXTZENKLL", "init1234"),
    SBL2("jdbc:oracle:thin:@//rztvnode005.cz.tmo:1523/Q2SBL01.WORLD", "EXTZENKLT", "inIT1234"),
    AMX1("jdbc:oracle:thin:@//hkpowt08.cz.tmo:1520/QAMECU01.WORLD", "tmcdb2", "tmcdb2"),
    AMX2("jdbc:oracle:thin:@//hkpowt08.cz.tmo:1520/QAMECU01.WORLD", "tmcdb3", "tmcdb3");

    private String databaseName;
    private String username;
    private String password;

    Database(String databaseName, String username, String password) {
        this.databaseName = databaseName;
        this.username = username;
        this.password = password;
    }

    public String getAddress() {
        return databaseName;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

}
