package sample;

import sample.enums.Database;

import java.sql.*;
import java.util.ArrayList;

public class DbConnector {

    public static String get(Database db, String select) {

        final String JDBC_DRIVER = "oracle.jdbc.driver.OracleDriver";
        Connection conn = null;
        Statement stmt = null;
        ArrayList<ArrayList<String>> result = new ArrayList<>();
        result.add(new ArrayList<>());

        try {

            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection(db.getAddress(), db.getUsername(), db.getPassword());
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(select);


            ResultSetMetaData rsmd = rs.getMetaData();
            int numCols = rsmd.getColumnCount();

            if (numCols > 1) {
                for (int j = 1; j <= numCols; j++) {
                    result.get(0).add(rsmd.getColumnName(j));
                }
            }
            int i = 1;
            while (rs.next()) {
                result.add(new ArrayList<>());
                for (int j = 1; j <= numCols; j++) {
                    result.get(i).add(rs.getString(j));
                }
                i++;
            }

            int rRows = result.size();
            int rCols = result.get(0).size();
            String[][] resultArr = new String[rRows][rCols];
            for (i = 0; i < rRows; i++) {
                for (int j = 0; j < rCols; j++) {
                    resultArr[i][j] = result.get(i).get(j);
                }
            }

            rs.close();
            stmt.close();
            conn.close();

            return ""+result.get(1);
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        int rRows = result.size();
        int rCols = result.get(0).size();
        String[][] resultArr = new String[rRows][rCols];
        int i = 0;
        for (i = 0; i < rRows; i++) {
            for (int j = 0; j < rCols; j++) {
                resultArr[i][j] = result.get(i).get(j);
            }
        }
        return ""+result.get(1);
    }

}
