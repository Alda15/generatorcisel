package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.util.StringConverter;
import sample.enums.Operators;

public class Controller {

    @FXML
    public void initialize()
    {
        HelpClass helpClass = new HelpClass();
        OkuGenerator okuGen = new OkuGenerator();
        resultArea.setEditable(false);

        operatorComboBox.getItems().setAll(Operators.values());
        operatorComboBox.setConverter(new StringConverter<Operators>() {

            @Override
            public String toString(Operators object) {
                switch (object) {
                    case TEST:
                        return "M_TEST";
                    case exGTS:
                        return "M_exGTS";
                    default:
                        break;
                }
                return null;
            }

            @Override
            public Operators fromString(String string) {
                return null;
            }
        });
    }

    @FXML
    private Button btnOKU;

    @FXML
    private Button btnBN;

    @FXML
    public ComboBox operatorComboBox;

    @FXML
    private ComboBox systemComboBox;

    @FXML
    private TextArea resultArea;


    @FXML
    private void getOku(ActionEvent event) {
        event.consume();
        //String OKU = HelpClass.generateOKU();
        resultArea.setText(operatorComboBox.getValue() + "\nCopied to Clipboard!");
    }

    @FXML
    private void getBN(ActionEvent event) {
        event.consume();
        String BN = HelpClass.generateBN();
        HelpClass.copyToClipboard(BN);
        resultArea.setText(BN + "\nCopied to Clipboard!");
    }

}