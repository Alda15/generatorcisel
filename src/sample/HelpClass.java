package sample;

import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;

public class HelpClass {

    public static void copyToClipboard(String clip) {

        final Clipboard clipboard = Clipboard.getSystemClipboard();
        final ClipboardContent content = new ClipboardContent();

        content.putString(clip);
        clipboard.setContent(content);
    }

    public static String generateBN(){
        BirthNumberGenerator genBN = new BirthNumberGenerator();
        String result = genBN.birthNumberGenerator();
        return result;
    }

    public static String generateOKU(){
        OkuGenerator genOKU = new OkuGenerator();
        String result = genOKU.okuGenerator();
        return result;
    }

}
