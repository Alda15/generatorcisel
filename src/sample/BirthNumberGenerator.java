package sample;

import java.math.BigInteger;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;

public class BirthNumberGenerator {

    public static String birthNumberGenerator() {

        String ceilingDate = "2000/01/01 00:00:00";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = null;
        try {
            date = sdf.parse(ceilingDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long ceilingDateInMillis = date.getTime();

        Date floorDate = new Date(0);
        long floorDateInMillis = floorDate.getTime();

        Long dateInMillis = ThreadLocalRandom.current().nextLong(floorDateInMillis, ceilingDateInMillis);
        String end = "" + ThreadLocalRandom.current().nextInt(100, 555);
        DateFormat df = new SimpleDateFormat("yyMMdd");
        Date generatedDate = new Date(dateInMillis);
        String generatedDateAsString = df.format(generatedDate);
        BigInteger num = new BigInteger(generatedDateAsString + end + "0");
        BigInteger parity = num.mod(BigInteger.valueOf(11));
        BigInteger res = (num.add(BigInteger.valueOf(11).subtract(parity))).abs();
        return "" + res;
    }
}
